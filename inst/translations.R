devtools::load_all()
saveRDS(
  as.data.frame(tibble::tribble(
    ~ namespace,	~ key, ~ lang, ~ translation,
    # "general", "test", "", "Test",
    # "general", "test", "de", "Der Test",
    "dashboard_table", VAR_NAMES, "", "Variable Names",
    # "dashboard_table", VAR_NAMES, "de", "Variablennamen"
    "dashboard_table", STUDY_SEGMENT, "", "Study Segment",
    "dashboard_table", LABEL, "", "Variable Label",
    "dashboard_table", "call_names", "", "tech. Call",
    "dashboard_table", "value", "", "Value",
    "dashboard_table", "values_raw", "", "tech. Value",
    "dashboard_table", "function_name", "", "Function Name",
    "dashboard_table", "indicator_metric", "", "tech. Indicator Metric",
    "dashboard_table", "class", "", "tech. Classification",
    "dashboard_table", "var_class", "", "Classification of the Variable",
    "dashboard_table", "Class", "", "Classification",
    "dashboard_table", "Metric", "", "Indicator Metric",
    "dashboard_table", "Call", "", "Call",
    "dashboard_table", "Graph", "", "Distribution",
    "dashboard_table", "Figure", "", "Related Plot",
  )), file = "inst/translations.rds", compress = "xz")

# Get all xlsx files delivered with dataquieR
get_xlsx_files <- function(path = "inst") {
  r <-
    file.path(path,
              list.files(path =
                           file.path(
                             rstudioapi::getActiveProject(),
                             path
                           ),
                         pattern = ".*\\.xlsx$",
                         ignore.case = TRUE,
                         full.names = !TRUE,
                         recursive = TRUE))
  rbuildignore <- readLines(file.path(
    rstudioapi::getActiveProject(),
    ".Rbuildignore")
  )
  ignore <- lapply(rbuildignore,
                   grepl, x = r, perl = TRUE)
  ignore <- apply(simplify2array(ignore), 1, any)
  r <- r[!ignore]
  r
}

# compress xlsx files
compress_xlsx <- function(files = get_xlsx_files("inst")) {
  base <- rstudioapi::getActiveProject()
  lapply(files, function(f) {
    !inherits(try({
      exdir <- tempfile()
      dir.create(exdir)
      old_dir <- setwd(exdir)
      zip::unzip(file.path(base, f))
      on.exit(setwd(old_dir))
      list.files(include.dirs = FALSE)
      zip::zip(file.path(base, f),
               list.files(include.dirs = FALSE),
               compression_level = 9,
                recurse = TRUE,
               include_directories = FALSE)
    }, silent = TRUE), "try-error")
  })
}

Dear CRAN Team,

I'm submitting an updated version (2.5.1) of our data quality assessment package
`dataquieR`. We have fixed a few bugs, most prominently one, that was detected
only by the latest developer version of R that came out almost at the same time
as `dataquieR` version 2.5.0 and was just not detected in all our release-tests. 
This is also, why we have an early update, now, sorry, we had tried our best.

We have addressed all issues shown at 

  https://cran.r-project.org/web/checks/check_results_dataquieR.html

with the current developer version of R. We also explicitly tested with a 
self-compiled developer version of R in a Fedora image, where we could reproduce
the error reported by CRAN.

We have also addressed a superfluous warning during package start-up and one
on installation, if certain suggested packages were absent. We have therefore
established a new pipeline for testing without suggested packages explicitly 
and also explicitly verifying the install.log for unexpected output.

We have tested the package on the platforms listed below. All reported 
0 warnings, 0 errors and only occasionally a note about the
time server not being reachable checking for future time stamps:
"NOTE unable to verify current time", which is not caused by our package:

  - R version 4.4.2 Patched (2025-01-18 r87618) (aarch64-apple-darwin20), local developer machine #1
  - R version 4.4.2 (2024-10-31 ucrt), (x86_64-w64-mingw32), local developer machine #2
  - R version 4.4.2 (2024-10-31 ucrt), (x86_64-w64-mingw32), local developer machine #3
  - R version 4.4.0 (2024-04-24), (x86_64-pc-linux-gnu), local developer machine #4
  - R version 4.4.2 Patched (2025-02-07 r87718) -- "Pile of Leaves" (x86_64-pc-linux-gnu)
  - R version 4.3.3 (2024-02-29) -- "Angel Food Cake" (x86_64-pc-linux-gnu)
  - R Under development (unstable) (2025-02-28 r87840) -- "Unsuffered Consequences" (x86_64-pc-linux-gnu)
  - R Under development (unstable) (2025-02-21 r87787) -- x86_64-pc-linux-gnu gcc (GCC) 12.2.1 20221121 (Red Hat 12.2.1-4) GNU Fortran (GCC) 12.2.1 20221121 (Red Hat 12.2.1-4)
  - WinBuilder, all 3 R versions
  - MacBuilder, both R versions
  - r-hub: Linux (standard plus addiontally the Redcap-Image), 
    Mac (both architectures) and Windows, see
    https://github.com/r-hub2/pictural-artificial-zander-dataquieR/actions

I hope, this time no bug is detected only a few days after this release.

Stephan
